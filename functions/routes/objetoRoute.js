module.exports = function(app) {

	var objetoController = app.controller.objetoController

	app.get('/listarObjetos', objetoController.listar)

	app.get('/consultarObjeto/:id' , objetoController.consultar)

	app.post('/listarObjetosUsuario', objetoController.listarObjetosUsuario)

    app.post('/cadastrarObjeto', objetoController.cadastrarObjeto)

    app.put('/alterarObjeto/:id', objetoController.alterar)

    app.delete('/removerObjeto/:id', objetoController.remover)

}