var mongoose = require('mongoose')

var usuarioSchema = mongoose.Schema({
    email: {
        type: String, 
        required: true,
        unique: true
    },
    senha: {
        type: String,
        required: true
    },
    nome: {
    	type: String,
    	required: true
    },
    whatsapp: {
    	type: String
    },
    facebook: {
    	type: String
    },   
    objetos: [{
        objeto:{    
            type: mongoose.Schema.Types.ObjectId, ref: 'objeto'
        }

    }],
    criadoEm: {
    	type: Date,
    	Default: Date.now
    },
    fotoURL: {
    	type: String
    }
})


exports = mongoose.model('usuario', usuarioSchema)