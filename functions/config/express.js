var express = require('express')
var consign = require('consign')
var bodyParser = require('body-parser')
var path = require('path')

var app = express()

app.set('secret', 'achareiodono') 
app.use(express.static('./public'))
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({

	extended: true
}))

consign()
    .include('models')
    .then('controller')
    .then('routes/usuarioRoute.js')
    .then('routes/auth.js')
    .then('routes')
    .into(app)

module.exports = app
