var mongoose = require('mongoose')
var usuarioController = {}
var model = mongoose.model('usuario')

module.exports = function(app) {

    usuarioController.listar = function(req, res, next){
        model.find()
        .then(function(result) {
            result.forEach(it => it.senha = null)
            res.json(result)
            return next()

        }, function(error) {
            res.send(error)
        })
    }

    usuarioController.gerarQRCode = function(req, res, next){
        if(!req.params.id){
            res.status(400).json({message : 'Você precisa informar o id do usuário'})
        }
        model.findOne({_id: req.params.id}).then(function(usuarioFound){
            if(!usuarioFound){
                res.status(404).json({message : 'Usuário não encontrado'})
                return
            }
            res.redirect('https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=https://us-central1-quemeodono.cloudfunctions.net/functions/consultarUsuarioHTML/' + usuarioFound._id)
        }).catch(function(error){
            console.log(error)
            res.status(500).json(error)
        })
    }

    usuarioController.consultar = function(req, res, next){
        if(!req.params.id){
            res.status(400).json({message : 'Você precisa informar o id do usuário'})
        }
        model.findOne({_id: req.params.id}).then(function(usuarioFound){
            if(!usuarioFound){
                res.status(404).json({message : 'Usuário não encontrado'})
                return
            }
            res.json(usuarioFound)
        }).catch(function(error){
            console.log(error)
            res.status(500).json(error)
        })
    }

    usuarioController.consultarHTML = function(req, res, next){
        if(!req.params.id){
            res.status(400).json({message : 'Você precisa informar o id do usuário'})
        }
        model.findOne({_id: req.params.id}).then(function(usuarioFound){
            if(!usuarioFound){
                res.status(404).json({message : 'Usuário não encontrado'})
                return
            }
            res.send(
            '<img src="'+usuarioFound.fotoURL+'" width="250px" heigth="250px"/>'+ 
            '<h4><b>Nome: </b> '+usuarioFound.nome+' </h4>'+
            '<h4><b>Email: </b> '+usuarioFound.email+' </h4>'+
            '<h4><b>Whatsapp: </b> '+usuarioFound.whatsapp+' </h4>'
            )
        }).catch(function(error){
            console.log(error)
            res.status(500).json(error)
        })
    }

    usuarioController.alterar = function(req, res, next){

        if(!req.params.id){
            res.status(400).json({message : 'Você precisa informar o id do usuário'})
        }

        var usuario = req.body

        model.update({ _id: req.params.id}, { $set: {'email': usuario.email , 'whatsapp': usuario.whatsapp , 'facebook': usuario.facebook , 'fotoURL' : usuario.fotoURL} }, function(usuarioUpdated){
            res.json({message : 'Usuário atualizado com sucesso!'})
        },
        function(error){
            console.log(error)
            res.status(500).json(error)
        })
    }


    usuarioController.remover = function(req, res , next){
        if(!req.params.id){
            res.status(400).json({message : 'Você precisa informar o id do usuário'})
        }
        model.find({_id: req.params.id},function(retorno){
            res.json({message : 'Usuário deletado com sucesso!'})
        }).remove().exec().catch(function(error){
            res.status(500).json(error)
        })
    }
    

    return usuarioController

}