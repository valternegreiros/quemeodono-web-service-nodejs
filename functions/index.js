const functions = require('firebase-functions')
const app = require('./config/express')
require('./config/database')(functions.config().mongodb != null ? functions.config().mongodb.url : 'mongodb://localhost:27017/test')

exports.functions = functions.https.onRequest(app)
