var express = require('express')
var mongoose = require('mongoose')
var jwt = require('jsonwebtoken')
var md5 = require('md5')

// API ROUTES -------------------

// get an instance of the router for api routes

module.exports = function (app) {

  var apiRoutes = express.Router()

  var model = mongoose.model('usuario')

  usuarioController = app.controller.usuarioController

  // route to authenticate a user (POST http://localhost:8080/api/authenticate)
  app.post('/cadastrar', function (req, res) {
    var usuario = req.body

    model.findOne({'email': usuario.email})
      .then(function (usuarioFound) {

        if(usuarioFound){
          res.status(400).json({message : "Usuário já cadastrado com esse email"})
          return
        }

        var token = jwt.sign(usuario, app.get('secret'))

        usuario.senha = md5(usuario.senha)
        usuario.criadoEm = new Date()
    
        model.create(usuario)
          .then(function (usuario) {
    
            res.set('jwt', token)
    
            usuario.senha = null
            usuario.token = token
    
            res.json(usuario)
    
          }, function (error) {
            console.log('não conseguiu')
            console.log(error)
            res.sendStatus(500)
          })

      }, function (error) {
        res.send(error)
      })

  })

  app.post('/login', function (req, res) {

    var usuario = req.body

    var token = jwt.sign(usuario, app.get('secret'))

    usuario.senha = md5(usuario.senha)

    model.findOne({ 'email': usuario.email, 'senha': usuario.senha })
      .then(function (usuario) {

        if (usuario) {
          res.set('jwt', token)

          usuario.senha = null

          res.json(usuario)
        }
        else {
          res.send('Usuario ou Senha Invalida')
        }


      }, function (error) {
        res.send(error)
      })

  })

  // route middleware to verify a token
  apiRoutes.use(function (req, res, next) {

    // check header or url parameters or post parameters for token
    var token = req.headers['jwt']

    // decode token
    if (token) {

      // verifies secret and checks exp
      jwt.verify(token, app.get('secret'), function (err, decoded) {
        if (err) {
          return res.json({ success: false, message: 'Token Inválido' })
        } else {
          // if everything is good, save to request for use in other routes
          req.decoded = decoded
          next()
        }
      })

    } else {
      // if there is no token
      // return an error
      return res.status(403).send({
        success: false,
        message: 'Nenhum Token enviado '
      })
    }
  })

  // apply the routes to our application with the prefix /api
  app.use('/', apiRoutes)

}