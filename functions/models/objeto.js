var mongoose = require('mongoose')
var Schema = mongoose.Schema

var schema = mongoose.Schema({
    nome: {
        type: String, 
        required: true
    },
    descricao: {
        type: String,
    },
    quantidade: {
    	type: Number
    },
    cor: {
        type: String
    },
    fotoURL: {
        type: String
    },
    idUsuario: {
        type: Schema.Types.ObjectId, 
        ref : 'usuario'
    }
})

exports = mongoose.model('objeto', schema)