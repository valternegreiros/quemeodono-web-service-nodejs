var mongoose = require('mongoose')
var jwt = require('jsonwebtoken')


var objetoController = {}

var modelUsuario = mongoose.model('usuario')
var modelObjeto = mongoose.model('objeto')

module.exports = function (app) {

    // Para listar todos os objetos eu nao preciso informar nada
    objetoController.listar = function (req, res, next) {
        modelObjeto.find().then(function (objetos) {
            res.json(objetos)
            return next()
        })
    }

    
    // Para consultar um objeto eu preciso informar o id do objeto
    objetoController.consultar = function (req, res, next) {

        if(!req.params.id){
            res.status(400).json({message : 'Você precisa informar o id do objeto na URL : /consultarObjeto/:id'})
        }

        modelObjeto.findOne({ _id: req.params.id}).then(function (objetoRetornado) {

            if (objetoRetornado == null) {
                res.status(404).json({message : 'Objeto não encontrado'})
            }
            else {
                res.json(objetoRetornado)
            }

            return next()

        })
    }


    // Para listar os objetos de um usuario eu preciso informar o idUsuario
    objetoController.listarObjetosUsuario = function (req, res, next) {

        var objeto = req.body

        modelUsuario.findOne({ _id: objeto.idUsuario }).then(function (usuarioEncontrado) {
            if (usuarioEncontrado == null) {
                res.status(404).json({message : 'Usuário não possui objetos'})
            }
            else {
                res.json(usuarioEncontrado.objetos)
            }
        })

    }

    // Para cadastrar um objeto eu preciso informar o idUsuario em quem devo inserir
    objetoController.cadastrarObjeto = function (req, res, next) {

        var objeto = req.body

        modelUsuario.findOne({ _id: objeto.idUsuario }).then(function (usuarioRetornado) {

            if (usuarioRetornado == null) {
                res.status(404).json({message : 'Usuario não encontrado'})
                return next()
            }
            else {

                modelObjeto.create(objeto)
                    .then(function (retorno) {

                        modelUsuario.findOne({ '_id': retorno.idUsuario }).then(function (usuario) {

                            usuario.objetos.push(retorno._id)

                            modelUsuario.findOneAndUpdate({ '_id': retorno.idUsuario }, usuario, { upsert: true }, function (erro, usuarioAtualizado) {

                                if (erro) {
                                    res.send(500, { error: err })
                                    return next()
                                }

                                res.json({message : 'Objeto cadastrado com sucesso'})
                                return next()
                            })

                        })


                    }, function (error) {
                        console.log('Aconteu um erro inesperado.')
                        res.send(error)
                        res.sendStatus(500)
                    })

            }

        })


    }

    // Para alterar o Objeto eu preciso informar o _id do objeto
    objetoController.alterar = function (req, res, next) {

        if(!req.params.id){
            res.status(400).json({message : 'Você precisa informar o id do objeto na URL : /alterarObjeto/:id'})
        }

        objeto = req.body

        modelObjeto.findOneAndUpdate({ '_id': req.params.id }, objeto, { upsert: true }).then(function (objetoRetornado) {
            res.json({message : 'Objeto atualizado com sucesso'})
            return next()
        })

    }


    // Para remover um objeto eu preciso informar o idUsuario e o _id do objeto.
    objetoController.remover = function (req, res) {

        if(!req.params.id){
            res.status(400).json({message : 'Você precisa informar o id do objeto na URL : /alterarObjeto/:id'})
        }

        modelUsuario.find().elemMatch("objetos", { "_id": req.params.id }).exec(function (err, usuarioEncontrado) {

            if (usuarioEncontrado[0] == null) {
                res.status(404).json({message : 'Objeto não encontrado'})
            }
            else {
                usuarioEncontrado[0].objetos.pull({ '_id': objeto._id })

                usuarioEncontrado[0].save()

                res.json(usuarioEncontrado[0])
            }
        })

        modelObjeto.remove({ _id: objeto._id }, function (err) {
            if (err) {
                res.json(err)
            }
        })
    }


    return objetoController

}