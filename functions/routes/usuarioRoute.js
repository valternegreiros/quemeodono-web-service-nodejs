module.exports = function(app) {

	var usuarioController = app.controller.usuarioController
    
    app.get('/listarUsuarios', usuarioController.listar)

    app.get('/gerarQRCode/:id', usuarioController.gerarQRCode)

    app.get('/consultarUsuario/:id', usuarioController.consultar)
    
    app.get('/consultarUsuarioHTML/:id', usuarioController.consultarHTML)

    app.put('/alterarUsuario/:id', usuarioController.alterar)

    app.delete('/removerUsuario/:id', usuarioController.remover)

}